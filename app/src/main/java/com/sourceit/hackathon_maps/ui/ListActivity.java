package com.sourceit.hackathon_maps.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.sourceit.hackathon_maps.App;
import com.sourceit.hackathon_maps.R;

public class ListActivity extends AppCompatActivity {

    RecyclerView list;

    static String recycler = "recycler";

    SharedPreferences sp = App.getApp().getSharedPreferences(recycler, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sp.edit();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        list = (RecyclerView) findViewById(R.id.list);
        // TODO: 15.03.2016 setAdapter
    }

    @Override
    protected void onResume() {
        super.onResume();
        editor.putBoolean(recycler, true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        editor.putBoolean(recycler, false);
    }

    // TODO: 15.03.2016 methodUpdateAdapter

}
