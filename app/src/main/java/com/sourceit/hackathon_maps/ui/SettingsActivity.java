package com.sourceit.hackathon_maps.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sourceit.hackathon_maps.R;

/**
 * Created by student on 15.03.2016.
 */
public class SettingsActivity extends Activity {

    EditText editTextForMessageLimit;
    EditText editTextForRadius;
    Button buttonForSettingChangesToFilters;
    Button buttonForCancelingAndReturningOnMap;
    View.OnClickListener onClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        editTextForMessageLimit = (EditText) findViewById(R.id.editTextForMessageLimit);
        editTextForRadius = (EditText) findViewById(R.id.editTextForRadius);

        // editTextForMessageLimit.setText(Data.messageLimit);
        // editTextForRadius.setText(Data.radius);

        buttonForSettingChangesToFilters = (Button) findViewById(R.id.buttonForSettingChangesToFilters);
        buttonForCancelingAndReturningOnMap = (Button) findViewById(R.id.buttonForCancelingAndReturningOnMap);
        onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.buttonForSettingChangesToFilters: {

                        // Data.limit = Integer.parseInt(editTextForMessageLimit.getText().toString());
                        // Data.radius = Integer.parseInt(editTextForMessageLimit.getText().toString());


                        Toast.makeText(SettingsActivity.this, "Изменения настроек сохранены", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                        startActivity(intent);

                        break;
                    }
                    case R.id.buttonForCancelingAndReturningOnMap: {
                        Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                        startActivity(intent);
                        break;
                    }
                }
            }
        };

    }
}
