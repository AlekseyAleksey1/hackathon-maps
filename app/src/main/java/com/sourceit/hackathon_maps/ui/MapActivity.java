package com.sourceit.hackathon_maps.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sourceit.hackathon_maps.R;

import static com.sourceit.hackathon_maps.ui.Data.messages;
import static com.sourceit.hackathon_maps.ui.Data.setMessage;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, ConnectionCallbacks, OnConnectionFailedListener {

    //server key AIzaSyAiQ5ZN411P4Fr4E_L5xAwgin5n5A9hsus

    GoogleApiClient googleApiClient;
    LocationManager locationManager;
    static GoogleMap myMap;
    public Location currentUsersLocation;
    public String usersLastMessage;

    EditText editTextForMessage;
    Button buttonForMessageSet;

    Button buttonForNormalOn;
    Button buttonForHybridOn;
    Button buttonForTerrainOn;
    View.OnClickListener mapTypesListener;

    Button buttonForChatOpening;
    Button buttonForSettingsOpening;
    View.OnClickListener controlButtonsListener;
    //ArrayList<MessageObject> messagesFromServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        buttonForNormalOn = (Button) findViewById(R.id.buttonForNormalType);
        buttonForHybridOn = (Button) findViewById(R.id.buttonForHybridType);
        buttonForTerrainOn = (Button) findViewById(R.id.buttonForTerrainType);

        mapTypesListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.buttonForNormalType: {
                        myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        break;
                    }
                    case R.id.buttonForHybridType: {
                        myMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        break;
                    }
                    case R.id.buttonForTerrainType: {
                        myMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                        break;
                    }
                }
            }
        };
        buttonForNormalOn.setOnClickListener(mapTypesListener);
        buttonForHybridOn.setOnClickListener(mapTypesListener);
        buttonForTerrainOn.setOnClickListener(mapTypesListener);

        buttonForChatOpening = (Button) findViewById(R.id.buttonForChatOpening);
        buttonForSettingsOpening = (Button) findViewById(R.id.buttonForSettingsOpening);
        controlButtonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.buttonForChatOpening: {
                        Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case R.id.buttonForSettingsOpening: {
                        Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(intent);
                        break;
                    }

                }
            }
        };
        buttonForChatOpening.setOnClickListener(controlButtonsListener);
        buttonForSettingsOpening.setOnClickListener(controlButtonsListener);

        editTextForMessage = (EditText) findViewById(R.id.editTextForMessage);
        buttonForMessageSet = (Button) findViewById(R.id.buttonForMessageSet);

        editTextForMessage.setVisibility(View.INVISIBLE);
        buttonForMessageSet.setVisibility(View.INVISIBLE);

        buttonForMessageSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTextForMessage.getText().length() == 0) {

                    Toast.makeText(MapActivity.this, "Введите сообщение", Toast.LENGTH_SHORT).show();

                } else {
                    usersLastMessage = editTextForMessage.getText().toString();

                    LatLng latLng = new LatLng(currentUsersLocation.getLatitude(), currentUsersLocation.getLongitude());
                    myMap.addMarker(new MarkerOptions().position(latLng).title(usersLastMessage)).showInfoWindow();

                    setMessage(usersLastMessage);
                }
            }
        });

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        googleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        myMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (location != null) {
            System.out.println("Provider" + LocationManager.GPS_PROVIDER + "has been selected");
            onLocationChanged(location);
        }

        myMap.setMyLocationEnabled(true);

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

        editTextForMessage.setVisibility(View.VISIBLE);
        buttonForMessageSet.setVisibility(View.VISIBLE);
    }


    @Override
    public void onLocationChanged(Location location) {

        currentUsersLocation = location;

       /* double lat = location.getLatitude();
        double lng = location.getLongitude();
        System.out.println("Location: " + lat + ", " + lng);
        LatLng latLng = new LatLng(lat, lng);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }*/
        // myMap.setMyLocationEnabled(true);
        // myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

    }

    static public void setMessagesOnMap() {

        // messagesFromServer = Data.messages;

        for (int i = 0; i < messages.size(); i++) {

            double lat = messages.get(i).lat;
            double lng = messages.get(i).lng;
            String name = messages.get(i).name;
            String message = messages.get(i).message;
            LatLng latLng = new LatLng(lat, lng);

            myMap.addMarker(new MarkerOptions().position(latLng).title(name + " сказал: " + message)).showInfoWindow();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sp = getSharedPreferences(Data.map, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(Data.map, true);
        editor.apply();

    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences sp = getSharedPreferences(Data.map, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(Data.map, false);
        editor.apply();

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
