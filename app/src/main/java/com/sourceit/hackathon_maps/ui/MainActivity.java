package com.sourceit.hackathon_maps.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sourceit.hackathon_maps.R;

public class MainActivity extends AppCompatActivity {

    public String userName;
    EditText editTextForUserName;
    Button buttonForLogginIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sp = getSharedPreferences("SPname", Context.MODE_PRIVATE);
        userName = sp.getString("name", "unknown");

        if (!userName.equalsIgnoreCase("unknown")) {
            Data.name = userName;
            Intent intent = new Intent(MainActivity.this, MapActivity.class);
            startActivity(intent);
        }

        editTextForUserName = (EditText) findViewById(R.id.editTextForUserName);
        buttonForLogginIn = (Button) findViewById(R.id.buttonForLogginIn);
        buttonForLogginIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTextForUserName.getText().length() == 0) {

                    Toast.makeText(MainActivity.this, "Вы не ввели имя", Toast.LENGTH_SHORT).show();

                } else {

                    userName = editTextForUserName.getText().toString();
                    Data.name = userName;
                    SharedPreferences sp = getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("name", userName);
                    editor.commit();

                    Intent intent = new Intent(MainActivity.this, MapActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
