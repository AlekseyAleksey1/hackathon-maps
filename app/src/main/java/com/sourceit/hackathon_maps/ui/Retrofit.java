package com.sourceit.hackathon_maps.ui;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;


/**
 * Created by Source_it-1 on 15.03.2016.
 */
public class Retrofit {
    private static final String ENDPOINT = "http://psi.kh.ua/hakaton";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/{request}")
        void getMessages(@Path(value = "request", encode = false) String name, Callback<List<MessageObject>> callback);


    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getMessages(double lat, double lng, int radius, int limit, Callback<List<MessageObject>> callback) {
        apiInterface.getMessages("api.php?lng=" + lng + "&lat=" + lat + "&radius=" + radius + "&limit=" + limit, callback);

    }

    public static void postMessage(String name, double lng, double lat, String message) {

    }
}
