package com.sourceit.hackathon_maps.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.widget.Toast;

import com.sourceit.hackathon_maps.App;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Source_it-1 on 15.03.2016.
 */
public class Data {

    static String map = "map";

    static SharedPreferences sp = App.getApp().getSharedPreferences(map, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sp.edit();

    static String name;

    //! array list add
    static List<MessageObject> messages;

    static int radius = 50;
    static int limit = 20;

    public static Location location;

    public static void setMessage(String text) {

    }

    public static void getMessages() {
        Retrofit.getMessages(location.getLatitude(), location.getLongitude(), radius, limit, new Callback<List<MessageObject>>() {
            @Override
            public void success(List<MessageObject> messagesPull, Response response) {
                messages = messagesPull;
                if (sp.getBoolean(map, false)) {
                    MapActivity.setMessagesOnMap();
                } else if (sp.getBoolean(ListActivity.recycler, false)) {
                    // TODO: 15.03.2016 UPD Adapter
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(App.getApp(), "something went wrong :(", Toast.LENGTH_SHORT).show();
            }
        });
    }
}