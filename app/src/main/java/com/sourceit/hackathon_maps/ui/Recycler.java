package com.sourceit.hackathon_maps.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sourceit.hackathon_maps.R;

import java.util.LinkedList;

public class Recycler extends RecyclerView.Adapter<Recycler.ViewHolder> {

    private LinkedList<String> objects;

    public Recycler(LinkedList<String> objects) {
        this.objects = objects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.message, parent, false);
        return new ViewHolder(v, objects);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(objects.get(position));

    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView time;
        private TextView message;

        public ViewHolder(View item, final LinkedList<String> objects) {
            super(item);
            name = (TextView) item.findViewById(R.id.textUser);
            time = (TextView) item.findViewById(R.id.textTime);
            message = (TextView) item.findViewById(R.id.textMessage);
        }
    }
}
